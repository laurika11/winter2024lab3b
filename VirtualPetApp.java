import java.util.Scanner;

public class VirtualPetApp{
	
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		Butterfly[] kaleidoscope = new Butterfly[4];
		
		for(int i=0; i<kaleidoscope.length; i++){
			kaleidoscope[i] = new Butterfly();
			System.out.println("Enter the colour of your butterfly.");
			kaleidoscope[i].colour = reader.nextLine();
			System.out.println("How old in minutes is your butterfly.");
			kaleidoscope[i].ageInMinutes = Integer.parseInt(reader.nextLine());
			System.out.println("Is your butterfly on a flower, true for yes or false for no.");
			kaleidoscope[i].onFlower = Boolean.parseBoolean(reader.nextLine());
		}
		
		System.out.println(kaleidoscope[kaleidoscope.length-1].colour);
		System.out.println(kaleidoscope[kaleidoscope.length-1].ageInMinutes);
		System.out.println(kaleidoscope[kaleidoscope.length-1].onFlower);
		
		kaleidoscope[0].canFly();
		kaleidoscope[0].canPollinate();
	}
}