public class Butterfly{
	
	public String colour;
	public int ageInMinutes;
	public boolean onFlower;
	
	public void canFly(){
		if(this.ageInMinutes < 0){
			System.out.println("Your butterfly hasn't hatched yet, be patient.");
		}
		else if(this.ageInMinutes == 0){
			System.out.println("Your butterfly is hatching, go watch it.");
		}
		else if(this.ageInMinutes <= 90){
			System.out.println("It is learning how to fly, wait a little.");
		}
		else{
			System.out.println("It can fly now!");
		}
	}
	public void canPollinate(){
		if(this.onFlower){
			System.out.println("It is pollinating.");
		}
		else{
			System.out.println("It isn't pollinating.");
		}
	}
}
	